package com.example.beautifulmalang;

import android.widget.ImageView;

public class RekomendasiWisataModel {

    String id, jarak, deskripsi;
    String image;

    public RekomendasiWisataModel() {
    }

    public RekomendasiWisataModel(String id, String jarak, String deskripsi, String image) {
        this.id = id;
        this.jarak = jarak;
        this.deskripsi = deskripsi;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getJarak() {
        return jarak;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getImage() {
        return image;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
