package com.example.beautifulmalang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    EditText et_username, et_password;
    Button bt_login;
    TextView tv_signup;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();
        mAuth = FirebaseAuth.getInstance();
        et_password =findViewById(R.id.log_tv_password);
        et_username = findViewById(R.id.log_tv_username);
        bt_login = findViewById(R.id.log_bt_login);
        tv_signup = findViewById(R.id.log_tv_signup);

        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);
            }
        });

        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String password, username;
                password = et_password.getText().toString();
                username = et_username.getText().toString();

                if (password.equals("") || username.equals("")){
                    Toast.makeText(LoginActivity.this, "Cant Empty!", Toast.LENGTH_SHORT).show();
                }else   {
                    mAuth.signInWithEmailAndPassword(username,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()){
                                Toast.makeText(getApplicationContext(),"Succes!", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(i);
                            }else {
                                Toast.makeText(getApplicationContext(),"FAILED!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });


    }
}