package com.example.beautifulmalang;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.net.URI;
import java.util.ArrayList;

public class RekomendasiWisataAdapter extends RecyclerView.Adapter<RekomendasiWisataAdapter.RekomendasiWisataViewHolder> {

    Context context;
    private final RecyclerViewInterface recyclerViewInterface;
    ArrayList<RekomendasiWisataModel> rekomendasiWisataModels;

    public RekomendasiWisataAdapter(Context context, ArrayList<RekomendasiWisataModel> rekomendasiWisataModels, RecyclerViewInterface recyclerViewInterface) {
        this.context = context;
        this.rekomendasiWisataModels = rekomendasiWisataModels;
        this.recyclerViewInterface = recyclerViewInterface;
    }

    @NonNull
    @Override
    public RekomendasiWisataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.rekomendasi_wisata_item, parent, false);
        return new RekomendasiWisataViewHolder(view, recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull RekomendasiWisataViewHolder holder, int position) {

        RekomendasiWisataModel rekomendasiWisataModel = rekomendasiWisataModels.get(position);
        // ambil image
        Glide.with(holder.itemView)
                .load(rekomendasiWisataModel.getImage())
                .transform(new CenterCrop(), new RoundedCorners(20))
                .into(holder.image);
        holder.id.setText(rekomendasiWisataModel.getId());
        holder.jarak.setText("~" + rekomendasiWisataModel.getJarak() + " km");
        holder.deskripsi.setText(rekomendasiWisataModel.getDeskripsi());

    }

    @Override
    public int getItemCount() {
        return rekomendasiWisataModels.size();
    }

    public static class RekomendasiWisataViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView id, jarak, deskripsi;

        public RekomendasiWisataViewHolder(@NonNull View itemView, RecyclerViewInterface recyclerViewInterface) {
            super(itemView);

            image = itemView.findViewById(R.id.img_wisata);
            id = itemView.findViewById(R.id.tv_tempat_wisata);
            jarak = itemView.findViewById(R.id.tv_jarak);
            deskripsi = itemView.findViewById(R.id.tv_deskripsi);

            itemView.setOnClickListener(view -> {
                if (recyclerViewInterface != null) {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        recyclerViewInterface.onItemClick(pos);
                    }
                }
            });
        }
    }
}
