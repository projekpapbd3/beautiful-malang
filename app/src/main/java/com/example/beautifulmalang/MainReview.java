package com.example.review;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.review.Review;
import com.example.review.adapter.ReviewAdapter;
import com.example.review.review.CreateActivity;
import com.example.review.review.UpdateActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainReview extends AppCompatActivity implements View.OnClickListener {
    private ListView listView;
    private ImageButton btnAdd;
    private ImageView btnBack;

    public static final String EXTRA_REVIEW = "extra_review";
    private Review review;
    private String database;

    private ReviewAdapter adapter;
    private ArrayList<Review> reviewList;
    DatabaseReference dbReview;
//    Intent getintent = getIntent();
//    String database = getintent.getStringExtra("database");
//    String database = "mahasiswa";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        review = getIntent().getParcelableExtra(EXTRA_REVIEW);

        if (review != null) {
            database = review.getId();
        } else {
            review = new Review();
        }

        dbReview = FirebaseDatabase.getInstance().getReference(database);

        listView = findViewById(R.id.lv_list);
        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnBack = findViewById(R.id.back);
        btnBack.setOnClickListener(this);

        reviewList = new ArrayList<>();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainReview.this, UpdateActivity.class);
                intent.putExtra(UpdateActivity.EXTRA_REVIEW, reviewList.get(i));
                intent.putExtra("database", database);

                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_add) {
            Intent intent = new Intent(MainReview.this, CreateActivity.class);
            intent.putExtra("database", database);
            startActivity(intent);
        } else if(view.getId() == R.id.back) {
            Intent in = new Intent(MainReview.this, DetailWisataActivity.class);
            startActivity(in);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        String email = currentUser.getEmail();
        dbReview.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                reviewList.clear();

                for (DataSnapshot reviewSnapshot : dataSnapshot.getChildren()) {
                    Review review = reviewSnapshot.getValue(Review.class);
                    reviewList.add(review);
                }

                ReviewAdapter adapter = new ReviewAdapter(MainReview.this);
                adapter.setReviewList(reviewList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(MainReview.this, "Terjadi kesalahan.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}