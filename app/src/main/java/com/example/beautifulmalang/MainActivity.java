package com.example.beautifulmalang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RecyclerViewInterface {

    LinearLayout btnSejarah, btnAlam, btnBudaya, btnKeluarga;
    public DrawerLayout drawerLayout;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    RecyclerView recyclerView;
    DatabaseReference database;
    RekomendasiWisataAdapter rekomendasiWisataAdapter;
    ArrayList<RekomendasiWisataModel> rekomendasiWisataModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSejarah = findViewById(R.id.btn_sejarah);
        btnAlam = findViewById(R.id.btn_alam);
        btnBudaya = findViewById(R.id.btn_budaya);
        btnKeluarga = findViewById(R.id.btn_keluarga);

        // intent ke list kota fragment
//        btnSejarah.setOnClickListener(v ->
//                startActivity(
//                        new Intent(MainActivity.this, KotaActivity.class)
//                )
//        );
//        btnAlam.setOnClickListener(v ->
//                startActivity(
//                        new Intent(MainActivity.this, KotaActivity.class)
//                )
//        );
//        btnBudaya.setOnClickListener(v ->
//                startActivity(
//                        new Intent(MainActivity.this, KotaActivity.class)
//                )
//        );
//        btnKeluarga.setOnClickListener(v ->
//                startActivity(
//                        new Intent(MainActivity.this, KotaActivity.class)
//                )
//        );

        recyclerView = findViewById(R.id.rv_rekomendasi_wisata);
        database = FirebaseDatabase.getInstance().getReference("/Kota/Malang/list");
//        database = FirebaseDatabase.getInstance().getReference("/Kota/"+reviewId+"/list/");
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        rekomendasiWisataModels = new ArrayList<>();
        rekomendasiWisataAdapter = new RekomendasiWisataAdapter(this, rekomendasiWisataModels, this);
        recyclerView.setAdapter(rekomendasiWisataAdapter);

        database.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    RekomendasiWisataModel rekomendasiWisataModel = dataSnapshot.getValue(RekomendasiWisataModel.class);
                    rekomendasiWisataModels.add(rekomendasiWisataModel);
                    Log.d("sukses", "Value is: " + rekomendasiWisataModel);
                }
                rekomendasiWisataAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("gagal", "Failed to read value.", error.toException());
            }
        });

        drawerLayout = findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, R.string.nav_open, R.string.nav_close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int position) {
//        Intent intent = new Intent(MainActivity.this, DetailWisataActivity.class);
    }
}